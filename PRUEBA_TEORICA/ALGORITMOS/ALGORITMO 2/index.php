<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE-edge">
        <title>VALIDAR CEDULA</title>

        <style>

            .center{
                text-align: center;
            }
        </style>
    </head>
    <body>
        <header>
            <h1 class="center">VALIDAR CEDULA</h1>
        </header>

        <section align="center">
            <form action="" method="GET">
                
                <div class="center">
                    <label for="example">Ingrese un numero</label>
                    <input type="number" name="dni" id="example">

                    <button type="submit" name="btn_validar" style="border-radius: 10%; background: #fff; padding: 8px"> VALIDAR</button>
                </div>

            </form>
        </section>

    </body>
</html>

<?php
    //SI SE PRESIONA EL BOTON GENERAR
    if(isset($_GET["btn_validar"])){
        $dni = $_GET["dni"];

        // Si tiene 10 dígitos es CEDULA
        // Si tiene 13 dígitos es RUC
        // Si empieza con 09, indicar que es de Guayas, caso contrario Otra Provincia
        // SI no cumple lo anterior, se debe mostrar que la cédula no es válida

        if(trim($dni) == ""){
            echo '<p class="center">Por favor ingrese su cedula</p>';
        }else{

            // $patron = "/^[[:digit:]]+$/";
            // if (!preg_match($patron, $dni)) {
            //     echo '<p class="center">Ingreso solo numeros </p>';
            //     return;
            // }

            $array = str_split($dni);
            //SE VALIDA SI TIENE MAS DE 13 DIGITOS O MENOS DE 10 DIGITOS
            if(sizeof($array) > 13 || sizeof($array) < 10){
                echo '<p class="center">IDENTIFICACION INVALIDA </p>';
            }else{
                //SI ES CEDULA
                if(sizeof($array) == 10){
                    $prov = (int) substr($dni,0,2);

                    if($prov == 9){
                        echo '<p class="center">ES CEDULA DE GUAYAQUIL</p>';
                    }else{
                        echo '<p class="center">ES CEDULA DE OTRA PROVINCIA </p>';
                    }
                    
                    return;
                }
                //SI ES RUC
                if(sizeof($array) == 13){

                    $prov = (int) substr($dni,0,2);

                    if($prov == 9){
                        echo '<p class="center">ES RUC DE GUAYAQUIL</p>';
                    }else{
                        echo '<p class="center">ES RUC DE OTRA PROVINCIA </p>';
                    }

                    return;
                }

            }




        }
        
    }
?>