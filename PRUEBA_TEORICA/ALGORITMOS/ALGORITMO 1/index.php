<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE-edge">
        <title>CARTAS</title>

        <style>

            .center{
                text-align: center;
            }
        </style>
    </head>
    <body>
        <header>
            <h1 class="center">CARTAS</h1>
        </header>

        <section align="center">
            <form action="" method="GET">

                <div class="center">
                    <button type="submit" name="btn_generar" style="border-radius: 10%; background: #fff; padding: 8px"> GENERAR</button>
                </div>

            </form>
        </section>

    </body>
</html>

<?php
    //SI SE PRESIONA EL BOTON GENERAR
    if(isset($_GET["btn_generar"])){
        // Palos: CORZACON_NEGRO, CORAZON_ROJO, DIAMANTE, TREBOL
        // Números: 2, 3, 4, 5, 6, 7, 8, 9, 10
        // Figuras: J, Q, K, AS

        $palos = [ 'CORZACON_NEGRO', 'CORAZON_ROJO' , 'DIAMANTE', 'TREBOL'];
        $figuras = ['J', 'Q', 'K', 'AS'];
        //numero - figura - numero - figura
        $i_palos = 0;
        for ($i=0; $i < 10 ; $i++) { 
            
            $output = '';
            $numero = rand(2, 10);

            // si es par es numero, si es impar es figura
            if (($i % 2) == 0){
                $output .= (string)$numero;
            }else{
                $key =  array_rand($figuras, 1);
                $output .= $figuras[$key];
            }   

            // si la variable indice para el array palos es mayor a 3 se setea 0
            if($i_palos > 3) $i_palos = 0;

            $output .= " ".$palos[$i_palos];
            echo "<p class='center'>".$output."</p>";

            $i_palos ++;
        }
        
    }
?>