# PROYECTOBACKEND


## Requerimientos
Tener instalado en CLI de symfony
Este proyecto esta realizado con MySql.
Tener php >=7.2.5


## Development server
En la constola ejecute `composer install`
Cree una base de datos con el nombre `turno_db`
Ejecutar en el consola de su base de datos en archivo `turno_db.sql` ubicado en la carpeta `PROYECTO_BACKEND`
En la consola ejecute `symfony serve:start`.