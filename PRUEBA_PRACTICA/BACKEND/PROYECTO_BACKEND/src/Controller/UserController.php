<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
        /**
     * @Route("/login", methods={"POST"})
    */
    public function login(Request $request, UserRepository $usuarioRepository): JsonResponse{
        try{
            $input = json_decode($request->getContent(), true);

            $usuario_existe = $usuarioRepository->findOneByUserName($input["user_name"]);
            if(!$usuario_existe){
                throw new Exception("Nombre de usuario incorrecto", 1);
            }

            if($usuario_existe->getPassword() != $input["password"]){
                throw new Exception("Password incorrecto", 1);
            }

            return new JsonResponse([
                "message"=> "Se ha procesado correctamente",
                "data"=>[
                    "id"=> $usuario_existe->getId(),
                    "tipo_user"=> $usuario_existe->getUserName(),
                ]
            ], 200);
        }catch(Exception $e){
            return new JsonResponse(["message"=>$e->getMessage()], 500);
        }
    }

    /**
     * @Route("/register", methods={"POST"})
    */
    public function register(Request $request, UserRepository $usuarioRepository): JsonResponse {
        try{
            $input = json_decode($request->getContent(), true);
            $usuario = new User();
            $usuario_existe = $usuarioRepository->findOneByUserName($input["user_name"]);
            
            if($usuario_existe){
                throw new Exception("El usuario ya existe", 1);
            }
            
            $usuario->setUserName($input["user_name"]);
            $usuario->setPassword($input["password"]);
            
            $usuarioRepository->add($usuario, true);
            return new JsonResponse(["message"=> "Se ha creado correctamente"], 201);
        }catch(Exception $e){
            return new JsonResponse(["message"=>$e->getMessage()], 500);
        }
    }
}
