<?php

namespace App\Controller;

use App\Entity\Turno;
use App\Repository\TurnoRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TurnoController extends AbstractController
{
    /**
     * @Route("/turno", methods={"GET"},name="app_turno")
     */
    public function list(TurnoRepository $turnoRepository): JsonResponse
    {   
        try{

            //$repository = $doctrine->getRepository(Turno::class);
            $turnos = $turnoRepository->findTurnos();

            $data = [];
            foreach ($turnos as $key => $value) {
                $data[] = [
                    "id" => $value->getId(),
                    "nombres" => $value->getNombres(),
                    "area" => $value->getArea(),
                    "tramite" => $value->getTramite(),
                    "observacion" => $value->getObservacion()
                ];
            }

            return new JsonResponse([
                "message"=> "Se ha procesado correctamente",
                "data"=> $data
            ], 200);

        }catch(Exception $e){
            return new JsonResponse(["message"=>$e->getMessage()], 500);
        }
    }

     /**
     * @Route("/historial", methods={"GET"})
     */
    public function historial(ManagerRegistry $doctrine): JsonResponse
    {   
        try{

            $repository = $doctrine->getRepository(Turno::class);
            $turnos = $repository->findAll();

            $data = [];
            foreach ($turnos as $key => $value) {
                $data[] = [
                    "id" => $value->getId(),
                    "nombres" => $value->getNombres(),
                    "area" => $value->getArea(),
                    "tramite" => $value->getTramite(),
                    "observacion" => $value->getObservacion(),
                    "is_despachado" => $value->isIsDespachado(),
                    "comentario" => $value->getComentario()
                ];
            }

            return new JsonResponse([
                "message"=> "Se ha procesado correctamente",
                "data"=> $data
            ], 200);

        }catch(Exception $e){
            return new JsonResponse(["message"=>$e->getMessage()], 500);
        }
    }

     /**
     * @Route("/turno", methods={"POST"})
     */
    public function save(Request $request,TurnoRepository $turnoRepository): JsonResponse{
        try{
            $input = json_decode($request->getContent(), true);

            $turno = new Turno();

            $turno->setNombres($input["nombres"]);
            $turno->setArea($input["area"]);
            $turno->setTramite($input["tramite"]);
            $turno->setObservacion($input["observacion"]);
            $turno->setIsDespachado(false);

            $turnoRepository->add($turno, true);
            return new JsonResponse(["message"=> "Se ha procesado correctamente"], 201);
        }catch(Exception $e){
            return new JsonResponse(["message"=>$e->getMessage()], 500);
        }
    }

    /**
     * @Route("/turno/{id}", methods={"PUT"})
     */
    public function despachar(ManagerRegistry $doctrine, int $id,Request $request){
        try{
            $input = json_decode($request->getContent(), true);
            $entityManager = $doctrine->getManager();
            $turno = $entityManager->getRepository(Turno::class)->find($id);

            $turno->setIsDespachado(true);
            $turno->setArchivo($input["archivo"]);
            $turno->setComentario($input["comentario"]);
            $entityManager->flush();

            // $url_insert = dirname(__FILE__) . "/files"; //Carpeta donde subiremos nuestros archivos

            // //Ruta donde se guardara el archivo, usamos str_replace para reemplazar los "\" por "/"
            // //$url_target = str_replace('\\', '/', $url_insert) . '/' . $file;

            // //Si la carpeta no existe, la creamos
            // if (!file_exists($url_insert)) {
            //     mkdir($url_insert, 0777, true);
            // };


            return new JsonResponse(["message"=> $input], 201);
        }catch(Exception $e){
            return new JsonResponse(["message"=>$e->getMessage()], 500);
        }
    }
}
