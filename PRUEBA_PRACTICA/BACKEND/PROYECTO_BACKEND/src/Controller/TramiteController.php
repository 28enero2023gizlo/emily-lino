<?php

namespace App\Controller;

use App\Entity\Tramite;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class TramiteController extends AbstractController
{
    /**
     * @Route("/tramite", methods={"GET"}, name="app_tramite")
     */
    public function list(ManagerRegistry $doctrine): JsonResponse
    {   
        try{

            $repository = $doctrine->getRepository(Tramite::class);
            $tramites = $repository->findAll();

            foreach ($tramites as $key => $value) {
                $data[] = [
                    "id" => $value->getId(),
                    "descripcion" => $value->getDescripcion()
                ];
            }

            return new JsonResponse([
                "message"=> "Se ha procesado correctamente",
                "data"=> $data
            ], 200);

        }catch(Exception $e){
            return new JsonResponse(["message"=>$e->getMessage()], 500);
        }
    
    }
}
