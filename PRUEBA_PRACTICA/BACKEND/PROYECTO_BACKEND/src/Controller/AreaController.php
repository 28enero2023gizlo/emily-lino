<?php

namespace App\Controller;

use App\Entity\Area;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AreaController extends AbstractController
{
    /**
     * @Route("/area", methods={"GET"}, name="app_area")
     */
    public function list(ManagerRegistry $doctrine): JsonResponse
    {   
        try{
            $repository = $doctrine->getRepository(Area::class);
            $areas = $repository->findAll();
            

            foreach ($areas as $key => $value) {
                $data[] = [
                    "id" => $value->getId(),
                    "descripcion" => $value->getDescripcion()
                ];
            }

            return new JsonResponse([
                "message"=> "Se ha procesado correctamente",
                "data"=> $data
            ], 200);

        }catch(Exception $e){
            return new JsonResponse(["message"=>$e->getMessage()], 500);
        }
    
    }

}
