import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ColaTurnosComponent } from './pages/cola-turnos/cola-turnos.component';
import { HistorialComponent } from './pages/historial/historial.component';
import { LoginComponent } from './pages/login/login.component';
import { TurnoComponent } from './pages/turno/turno.component';
import { VentanillaComponent } from './pages/ventanilla/ventanilla.component';
import { SharedComponent } from './shared/shared.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'menu',
    component: SharedComponent,
    children: [
      { path: 'turno', component: TurnoComponent },
      { path: 'cola', component: ColaTurnosComponent },
      { path: 'ventanilla', component: VentanillaComponent },
      { path: 'historial', component: HistorialComponent },
    ]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
