import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.css']
})
export class SharedComponent implements OnInit {

  usuario_id: string | null = sessionStorage.getItem('usuario_id');
  loading: boolean = true;
  constructor() { }

  ngOnInit(): void {
  }

}
