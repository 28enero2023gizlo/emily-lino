export class Turno {
    constructor(
        public readonly id: string = "",
        public nombres: string = "",
        public area: string = "",
        public tramite: string = "",
        public observacion: string = "",
        public is_despachado: string = "",
        public comentario: string = "",
        public archivo: any
    ){}
}