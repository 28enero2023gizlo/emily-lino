import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzSpaceModule } from 'ng-zorro-antd/space';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { SharedComponent } from './shared/shared.component';
import { TurnoComponent } from './pages/turno/turno.component';
import { NzAlertModule } from 'ng-zorro-antd/alert';
import { ColaTurnosComponent } from './pages/cola-turnos/cola-turnos.component';
import { VentanillaComponent } from './pages/ventanilla/ventanilla.component';
import { LoginComponent } from './pages/login/login.component';
import { HistorialComponent } from './pages/historial/historial.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    SharedComponent,
    TurnoComponent,
    ColaTurnosComponent,
    VentanillaComponent,
    LoginComponent,
    HistorialComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzButtonModule,
    NzIconModule,
    NzLayoutModule,
    NzMenuModule,
    NzBreadCrumbModule,
    NzPageHeaderModule,
    NzDividerModule,
    NzGridModule,
    NzFormModule,
    NzSpinModule,
    NzInputModule,
    NzNotificationModule,
    NzSpaceModule,
    NzToolTipModule,
    NzTableModule,
    NzCardModule,
    NzModalModule,
    NzTabsModule,
    NzBadgeModule,
    NzSelectModule,
    ReactiveFormsModule,
    NzAlertModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
