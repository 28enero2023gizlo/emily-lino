import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { ApiResponse } from "../models/api-response";
import { Turno } from "../models/turno";

@Injectable({
    providedIn: 'root'
})
export class TurnoService{

    constructor(
        private http: HttpClient
    ){}

    getAll(qs: string){
        const url = `${environment.api_host}turno${qs}`;
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        const options =  { headers };

        return this.http.get<ApiResponse>(url, options);
    }


    historial(qs: string){
        const url = `${environment.api_host}historial${qs}`;
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        const options =  { headers };

        return this.http.get<ApiResponse>(url, options);
    }


    save(data: Turno,qs: string){
        const url = `${environment.api_host}turno${qs}`;
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        const options =  { headers };

        return this.http.post<ApiResponse>(url, data ,options);
    }

    update(turno: Turno, data: any){
        const url = `${environment.api_host}turno/${turno.id}`;
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        const options =  { headers };

        return this.http.put<ApiResponse>(url, data ,options);
    }
} 