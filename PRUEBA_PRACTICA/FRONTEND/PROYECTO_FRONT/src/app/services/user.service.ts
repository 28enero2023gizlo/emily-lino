import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiResponse } from '../models/api-response';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  login(data: any){
    const url = `${environment.api_host}login`;
		const headers = new HttpHeaders({
      'Accept': 'application/json',
    });
		const options =  { headers };

    return this.http.post<ApiResponse>(url, data, options).pipe(
     
    );
  }

  register(data: any){
    const url = `${environment.api_host}register`;
		const headers = new HttpHeaders({
      'Accept': 'application/json',
    });
		const options =  { headers };

    return this.http.post<ApiResponse>(url, data, options);
  }

}
