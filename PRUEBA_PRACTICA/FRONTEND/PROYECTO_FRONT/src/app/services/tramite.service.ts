import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { ApiResponse } from "../models/api-response";

@Injectable({
    providedIn: 'root'
})
export class TramiteService {
    constructor(
        private http: HttpClient
    ){}

    getAll(qs: string = ""){
        const url = `${environment.api_host}tramite${qs}`;
        const headers = new HttpHeaders({
            'Accept': 'application/json',
        });
        const options =  { headers };

        return this.http.get<ApiResponse>(url, options);
    }
}