import { Component, OnInit } from '@angular/core';
import { Turno } from 'src/app/models/turno';
import { TurnoService } from 'src/app/services/turno.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {

  loading: boolean = false;
  turnos: Turno[];
  dataFound: string = '';
  constructor(
    private turnoService: TurnoService
  ) { 
    this.turnos = [];
  }

  ngOnInit(): void {
    this.getTurnos();
  }

  getTurnos(){
    try{
      this.loading =  false;
      this.turnoService.historial("").subscribe(
        (res)=>{
          this.turnos = res.data;
          this.loading = false;
        }
      )
    }catch(err){
      this.loading = false;
    }
  }
}
