import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading: boolean = false;
  login_form!: FormGroup;

  register_mode: boolean;
  constructor(
      private fb: FormBuilder,
      private usuarioService: UserService,
      private notification: NzNotificationService,
      private router: Router,
  ) { 
      this.register_mode = false;
      this.onForm();
  }

  ngOnInit(): void {
  }

  onForm(){
    this.login_form = this.fb.group({
        user_name: ["",[Validators.required]],
        password: ["", [Validators.required]]
    });
    
  }

  onChangeMode(){
    this.register_mode = !this.register_mode;
    this.onForm();
  }

  onSubmit(){
    try{
        this.loading = true;
        const data_form = this.login_form.getRawValue();

        if(this.register_mode){

            this.usuarioService.register(data_form).subscribe(
                (_)=> { 
                    this.loading = false;
                    this.notification.create(
                        'success',
                        'Guardado',
                        'Se ha guardado correctamente.',
                        { nzPlacement: 'bottomRight' }
                    );
                }
            );

        }else{
            this.usuarioService.login(data_form).subscribe(
                (data)=> { 
                    const user: User = data.data;
                    sessionStorage.setItem('logged', 'in');
                    sessionStorage.setItem('usuario_id', user.id);
                    sessionStorage.setItem('user_name', user.user_name);
                    sessionStorage.setItem('tipo_user', user.tipo_user);
                    this.loading = false;

                    this.router.navigate(['menu']);
                    
                }
            );
        }

    }catch(err){
        this.loading = false;
    }
}


}
