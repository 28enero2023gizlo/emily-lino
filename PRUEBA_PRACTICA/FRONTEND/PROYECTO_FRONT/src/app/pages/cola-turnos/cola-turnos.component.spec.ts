import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColaTurnosComponent } from './cola-turnos.component';

describe('ColaTurnosComponent', () => {
  let component: ColaTurnosComponent;
  let fixture: ComponentFixture<ColaTurnosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColaTurnosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColaTurnosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
