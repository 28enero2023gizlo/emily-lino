import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Area } from 'src/app/models/area';
import { Tramite } from 'src/app/models/tramite';
import { AreaService } from 'src/app/services/area.service';
import { TramiteService } from 'src/app/services/tramite.service';
import { TurnoService } from 'src/app/services/turno.service';

@Component({
  selector: 'app-turno',
  templateUrl: './turno.component.html',
  styleUrls: ['./turno.component.css']
})
export class TurnoComponent implements OnInit {

  loading: boolean = false;
  turno_form!: FormGroup;
  areas: Area[];
  tramites: Tramite[];
  constructor(
    private fb: FormBuilder,
    private areaService: AreaService,
    private tramiteService: TramiteService,
    private turnoService: TurnoService,
    private notification: NzNotificationService
  ) { 
    this.areas = [];
    this.tramites = [];
  }

  async ngOnInit() {
    this.onForm();
    await this.getData();
  }

  async getData(){
    try{
      this.loading = true;
      await this.getAreas();
      await this.getTramite();

      this.loading = false;
    }catch(err){
      this.loading = false;
    }
  }

  getAreas(): Promise<void>{
    return new Promise((resolve)=>{
      this.areaService.getAll().subscribe(
        (res)=>{
          this.areas = res.data;
          resolve();
        }
      );
    });
  }

  getTramite(): Promise<void>{
    return new Promise((resolve)=>{
      this.tramiteService.getAll().subscribe(
        (res)=>{
          this.tramites = res.data;
          console.log(res.data)
          resolve();
        },
      );
    });
  }

  onForm(){
    this.turno_form = this.fb.group({
      nombres: ['',[Validators.required]],
      area: ['',[Validators.required]],
      tramite: ['',[Validators.required]],
      observacion: ['',[Validators.required]],
    });
  }

  onSubmit(){
    try{
      this.loading = true;
      const data  = this.turno_form.getRawValue();

      this.turnoService.save(data,"").subscribe(
        (_)=>{
          this.loading = false;
          this.onForm();
          this.notification.create(
            'success',
            'Guardado',
            'Se ha guardado correctamente.',
            { nzPlacement: 'bottomRight' }
        );
        }
      )
    }catch(err){
      this.loading = false;
    }
  }

}
