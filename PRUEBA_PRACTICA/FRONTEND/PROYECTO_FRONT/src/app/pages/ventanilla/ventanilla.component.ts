import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Turno } from 'src/app/models/turno';
import { TurnoService } from 'src/app/services/turno.service';

@Component({
  selector: 'app-ventanilla',
  templateUrl: './ventanilla.component.html',
  styleUrls: ['./ventanilla.component.css']
})
export class VentanillaComponent implements OnInit {

  loading: boolean = false;
  turnos: Turno[];
  turno_form!: FormGroup;
  index: number = 0;
  file: any = {};
  constructor(
    private fb: FormBuilder,
    private turnoService: TurnoService,
    private notification: NzNotificationService
  ) { 
    this.turnos = [];
  }

  ngOnInit(): void {
    this.onForm();
    this.getTurnos();
  }

  onForm(){
    this.turno_form = this.fb.group({
      id: [''],
      nombres: [{value: '', disabled: true},[Validators.required]],
      area: [{value: '', disabled: true},[Validators.required]],
      tramite: [{value: '', disabled: true},[Validators.required]],
      observacion: [{value: '', disabled: true},[Validators.required]],
      comentario: ['',[Validators.required]],
    });
  }

  setForm(){
    this.turno_form.patchValue({
      nombres: this.turnos[this.index].nombres,
      area: this.turnos[this.index].area,
      tramite: this.turnos[this.index].tramite,
      observacion: this.turnos[this.index].observacion,
    });
  }

  getTurnos(){
    try{
      this.loading =  false;
      this.turnoService.getAll("").subscribe(
        (res)=>{
          this.turnos = res.data;

          if(this.turnos.length > 0 ){
            this.index = 0;
            this.setForm();
          }

          this.loading = false;
        }
      )
    }catch(err){
      this.loading = false;
    }
  }

  onSubmit(){
    try{
      this.loading = true;
      const {comentario} = this.turno_form.getRawValue();

      this.turnos[this.index].comentario = comentario;
      this.turnos[this.index].archivo = this.file.name;

      // const formData = new FormData(); 
		  // formData.append('archivo', this.file, this.file.name);
		  // formData.append('comentario', String(comentario));

      this.turnoService.update(this.turnos[this.index], this.turnos[this.index]).subscribe(
        (res)=>{
          this.loading = false;

          this.notification.create(
            'success',
            'Guardado',
            'Se ha guardado correctamente.',
            { nzPlacement: 'bottomRight' }
          );
          this.index = 0;
          this.getTurnos();
        }
      )

    }catch(err){
      this.loading = false;
    }
  }

  uploadFile(event: any){
		this.file = event.target.files[0];		
	}

  saltar(){
    this.index++;
    this.setForm();
  }

}
